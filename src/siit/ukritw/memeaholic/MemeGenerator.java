package siit.ukritw.memeaholic;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import th.ac.tu.siit.lab8webservice.R;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

public class MemeGenerator extends Activity implements OnClickListener {

	private Spinner memeSpinner;
	private ImageView imView;
	public Bitmap memeCreated = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_meme_generator);

		imView = (ImageView) findViewById(R.id.imView);

		memeSpinner = (Spinner) findViewById(R.id.spinner1);
		// set default spinner value 
		memeSpinner.setSelection(0);

		Button generate = (Button) findViewById(R.id.generate);
		generate.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.meme_generator, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId(); 
		switch(id) {
		case R.id.action_share:
			if (memeCreated != null) {
				File sdCardDirectory = Environment.getExternalStorageDirectory();
				File image = new File(sdCardDirectory, "tempMeme.jpg");

				FileOutputStream outStream;
				try {
					outStream = new FileOutputStream(image);
					memeCreated.compress(Bitmap.CompressFormat.JPEG, 100, outStream); 

					outStream.flush();
					outStream.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				Intent share = new Intent(Intent.ACTION_SEND);
				share.setType("image/jpeg");
				String string = getString(R.string.file_sdcard_tempmeme_jpg);
				share.putExtra(Intent.EXTRA_STREAM, Uri.parse(string));
				startActivity(Intent.createChooser(share, "Share Image"));
			}
			return true;
		default:
			return super.onOptionsItemSelected(item);
		} 
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();

		switch(id) {
		case R.id.generate:
			EditText topTextET = (EditText) findViewById(R.id.topText);
			EditText bottomTextET = (EditText) findViewById(R.id.bottomText);

			String meme = String.valueOf(memeSpinner.getSelectedItem());
			String topText = "";
			String bottomText = "";
			try {
				meme = java.net.URLEncoder.encode(meme, "UTF-8");
				topText = java.net.URLEncoder.encode(topTextET.getText().toString(), "UTF-8");
				bottomText = java.net.URLEncoder.encode(bottomTextET.getText().toString(), "UTF-8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


			String url = "http://apimeme.com/meme?meme="+meme+
					"&top="+topText+
					"&bottom="+bottomText;
			new DownloadImageTask(imView).execute(url);
			imView.setVisibility(View.VISIBLE);
			break;
		}

	}

	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		ImageView bmImage;

		public DownloadImageTask(ImageView bmImage) {
			//initialize variable with the passing ImageView parameter
			this.bmImage = bmImage;
		}

		protected void onPreExecute() {
			Toast t = Toast.makeText(getApplicationContext(), 
									"Generating Meme", Toast.LENGTH_SHORT);
			t.show();

		}

		protected Bitmap doInBackground(String... urls) {
			//get url of image
			String urldisplay = urls[0];
			//create Bitmap variable
			Bitmap img = null;
			try {
				//create instance of InputStream and pass URL
				InputStream in = new java.net.URL(urldisplay).openStream();
				//decode stream and initialize bitmap 
				img = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			memeCreated = img;
			return img;
		}

		protected void onPostExecute(Bitmap result) {
			//get bitmap and initialize ImageView
			bmImage.setImageBitmap(result);
			memeCreated = result;
		}
	}

}
